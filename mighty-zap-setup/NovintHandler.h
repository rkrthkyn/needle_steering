#include <chai3d.h>
#include <gl/glew.h>
#include <gl/GL.h>
#include <gl/freeglut.h>


using namespace chai3d;

namespace NovintHandler {

	class novintHandler
	{
	public:
		// a haptic device handler
		cHapticDeviceHandler * handler;

		// a pointer to the current haptic device
		cGenericHapticDevicePtr hapticDevice;

		// flag to indicate if the haptic simulation currently running
		bool simulationRunning;

		// flag to indicate if the haptic simulation has terminated
		bool simulationFinished;

		// frequency counter to measure the simulation haptic rate
		cFrequencyCounter frequencyCounter;

		// haptic thread
		cThread* hapticsThread;

		static void updateHaptics(void);

		novintHandler initializeHapticThread(void);

	};
}



