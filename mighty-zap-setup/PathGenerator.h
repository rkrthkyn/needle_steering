#include <stdlib.h>
#include <iostream>
#include <sstream>

using namespace std;

namespace PathGenerator {
	class pathGenerator {

	public:

		double function(double x, double y, double z, double goal[], double obstacles[],int size);
		double step(double x);
	};
}

