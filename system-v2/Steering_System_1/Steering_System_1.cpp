/*--------------------------------------------------
Rohith Karthikeyan - rohithkarthikeyan93@gmail.com
Steering Manipulator Control

Update Log:

10/25/2017 - Dynamic Testing Features

09 / 07 / 2017 - get servos to move in right direction
Correct for servo travel during servo group change
Limit extent of travel
Servo directions :
Starting positions and Movement :
	Servos organized in the order of proximity to needle
	Servo 0 - 90	90 - 0
	Servo 1 - 0	0 - 90
	Servo 2 - 90 90 - 0
	Servo 3 - 0	0 - 90

update : 08 / 18 / 2017 - code integration completed
Requires testing

----------------------------------------------------*/
#include "stdafx.h"
#include <sdkddkver.h>
#include <windows.h>
#include <stdio.h>
#include <conio.h>
#include <phidget21.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>
#include <tchar.h>
#include <iomanip>
#include <sstream>
#include <iostream>
#include <sstream>
#include <string>
#include <fstream>

using namespace std;

//==================================
//Stepper Variables Begin

int i1 = 1, i2 = 1;
double const newConv = 640;
double insDepth;
int boolStepper = 1;
double const distStepper = 1;
__int64 stepperCurrentPosition;
double joyStartStepper, joyNewStepper;

//Stepper Variables End
//==================================

//==================================
//Servo Variables Start

double const distServo = 10;
double rotServo0, rotServo1, rotServo2, rotServo3;
bool boolEnable = true;
int servoFlag0 = 1, servoFlag1 = 0, servoFlag2 = 0, servoFlag3 = 0;
double servo1CurrentPosition, servo0CurrentPosition, servo2CurrentPosition, servo3CurrentPosition;
double joyNewServoGroup1, joyNewServoGroup2;
double joyStartServoGroup1 = 0, joyStartServoGroup2 = 0;
int boolServoGroupChange = 0;
bool runDynamics = true;

// Servo Variables End
//==================================

//==================================
//Phidget Handles Begin

CPhidgetStepperHandle stepper;
CPhidgetInterfaceKitHandle ifkit;
CPhidgetAdvancedServoHandle servo;

// Phidget Handles End
//==================================

//==================================
// User defined functions begin

// Stepper position control function

void Insertion_Control()
{
	if (joyNewStepper > joyStartStepper) // If the current Joystick value is more than the starting value
	{
		insDepth = stepperCurrentPosition + distStepper*i1*newConv;
		joyStartStepper = joyNewStepper;
		if (insDepth == stepperCurrentPosition) // untile position is reached
		{
			i1++;
		}
	}
	else if (joyNewStepper < joyStartStepper) // If current posn < the start val - do something else
	{
		insDepth = stepperCurrentPosition - distStepper*i2*newConv;
		joyStartStepper = joyNewStepper;
		if (insDepth == stepperCurrentPosition)
		{
			i2++;
		}
	}
	else // no change in joystick position
	{
		insDepth = 0;
		joyStartStepper = joyNewStepper;
	}
	cout << "Target_position:" << insDepth << endl;
}

// Servo rotation control function
void rotationControlGroup1()
{
	if (joyNewServoGroup1 > joyStartServoGroup1)
	{
		if (servoFlag0 == 1)
		{
			rotServo0 = servo0CurrentPosition + distServo;
			joyStartServoGroup1 = joyNewServoGroup1;
		}

		else if (servoFlag1 == 1)
		{
			rotServo1 = servo1CurrentPosition + distServo;
			joyStartServoGroup1 = joyNewServoGroup1;
		}

	}

	else if (joyNewServoGroup1 < joyStartServoGroup1)
	{
		if (servoFlag0 == 1)
		{
			rotServo0 = servo0CurrentPosition - distServo;
			joyStartServoGroup1 = joyNewServoGroup1;
		}

		else if (servoFlag1 == 1)
		{
			rotServo1 = servo1CurrentPosition - distServo;
			joyStartServoGroup1 = joyNewServoGroup1;
		}

	}

	else

	{
		cout << "No change\n";
		rotServo0 = 0;
		rotServo1 = 0;
		joyStartServoGroup1 = joyNewServoGroup1;
	}



}
void rotationControlGroup2()
{
	if (joyNewServoGroup2 > joyStartServoGroup2)
	{
		if (servoFlag2 == 1)
		{
			rotServo2 = servo2CurrentPosition + distServo;
			joyStartServoGroup2 = joyNewServoGroup2;
		}

		else if (servoFlag3 == 1)
		{
			rotServo3 = servo3CurrentPosition + distServo;
			joyStartServoGroup2 = joyNewServoGroup2;
		}

	}

	else if (joyNewServoGroup2 < joyStartServoGroup2)
	{
		if (servoFlag2 == 1)
		{
			rotServo2 = servo2CurrentPosition - distServo;
			joyStartServoGroup2 = joyNewServoGroup2;
		}

		else if (servoFlag3 == 1)
		{
			rotServo3 = servo3CurrentPosition - distServo;
			joyStartServoGroup2 = joyNewServoGroup2;
		}

	}

	else

	{
		cout << "No change\n";
		rotServo0 = 0;
		rotServo1 = 0;
		rotServo2 = 0;
		rotServo3 = 0;
		joyStartServoGroup1 = joyNewServoGroup1;
	}



}

// User defined functions end
//==================================

// ============ Call Backs Stepper Begin ================

int CCONV AHandler(CPhidgetHandle stepper, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(stepper, &name);
	CPhidget_getSerialNumber(stepper, &serialNo);

	return 0;
}

int CCONV DHandler(CPhidgetHandle stepper, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(stepper, &name);
	CPhidget_getSerialNumber(stepper, &serialNo);

	return 0;
}

int CCONV EHandler(CPhidgetHandle stepper, void *userptr, int ErrorCode, const char *Description)
{
	printf("Error handled. %d - %s\n", ErrorCode, Description);
	return 0;
}

int CCONV PCHandler(CPhidgetStepperHandle stepper, void *usrptr, int Index, __int64 Value)
{
	return 0;
}

int DP(CPhidgetStepperHandle phid)
{
	int serialNo, version, numMotors;
	const char* ptr;

	CPhidget_getDeviceType((CPhidgetHandle)phid, &ptr);
	CPhidget_getSerialNumber((CPhidgetHandle)phid, &serialNo);
	CPhidget_getDeviceVersion((CPhidgetHandle)phid, &version);

	CPhidgetStepper_getMotorCount(phid, &numMotors);


	printf("%s\n", ptr);
	printf("Serial Number: %10d\nVersion: %8d\n# Motors: %d\n", serialNo, version, numMotors);

	return 0;
}

// ============ Call Backs Stepper End ================

// ============ Call Backs IFK Begin ==================

int CCONV attachHandler(CPhidgetHandle IFK, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(IFK, &name);
	CPhidget_getSerialNumber(IFK, &serialNo);

	printf("%s %10d attached!\n", name, serialNo);

	return 0;
}
int CCONV detachHandler(CPhidgetHandle IFK, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(IFK, &name);
	CPhidget_getSerialNumber(IFK, &serialNo);

	printf("%s %10d detached!\n", name, serialNo);

	return 0;
}

int CCONV errorHandler(CPhidgetHandle IFK, void *userptr, int ErrorCode, const char *unknown)
{
	printf("Error handled. %d - %s", ErrorCode, unknown);
	return 0;
}

int CCONV inputChangeHandler(CPhidgetInterfaceKitHandle IFK, void *usrptr, int Index, int State)
{
	printf("Digital Input: %d > State: %d\n", Index, State);

	// Stops system run
	if (Index == 0 && State ==1) {
		boolEnable = !boolEnable;
		}

	// Shifts between active servos

	if (Index == 1 && State == 0)
	{
		servoFlag0 = 1;
		servoFlag1 = 0;
		runDynamics = false;
	}

	else if (Index == 1 && State == 1)
	{
		servoFlag0 = 0;
		servoFlag1 = 1;
		runDynamics = true;
		
	}

	if (Index == 2 && State == 0)
	{
		servoFlag2 = 1;
		servoFlag3 = 0;
	}

	else if (Index == 2 && State == 1)
	{
		servoFlag2 = 0;
		servoFlag3 = 1;

	}

	if (Index == 3 && State == 0)
	{
		boolStepper = 0;
	}
	else if (Index == 3 && State == 1)
	{
		boolStepper = 1;
	}


	return 0;
}

int CCONV outputChangeHandler(CPhidgetInterfaceKitHandle IFK, void *usrptr, int Index, int State)
{
	printf("Digital Output: %d > State: %d\n", Index, State);
	return 0;
}

int CCONV sensorChangeHandler(CPhidgetInterfaceKitHandle IFK, void *usrptr, int Index, int Value)
{
	if (Index == 0) {
		joyNewStepper = Value;
		Insertion_Control();
		joyStartStepper = joyNewStepper;
	}
	else if (Index == 1) {
		joyNewServoGroup1 = Value;
		rotationControlGroup1();
	}

	else if (Index == 2)
	{
		joyNewServoGroup2 = Value;
		rotationControlGroup2();
	}
	return 0;
}

int Display_Properties(CPhidgetInterfaceKitHandle phid)
{
	int serialNo, version, numInputs, numOutputs, numSensors, triggerVal, ratiometric, i;
	const char* ptr;

	CPhidget_getDeviceType((CPhidgetHandle)phid, &ptr);
	CPhidget_getSerialNumber((CPhidgetHandle)phid, &serialNo);
	CPhidget_getDeviceVersion((CPhidgetHandle)phid, &version);

	CPhidgetInterfaceKit_getInputCount(phid, &numInputs);
	CPhidgetInterfaceKit_getOutputCount(phid, &numOutputs);
	CPhidgetInterfaceKit_getSensorCount(phid, &numSensors);
	CPhidgetInterfaceKit_getRatiometric(phid, &ratiometric);

	printf("%s\n", ptr);
	printf("Serial Number: %10d\nVersion: %8d\n", serialNo, version);
	printf("# Digital Inputs: %d\n# Digital Outputs: %d\n", numInputs, numOutputs);
	printf("# Sensors: %d\n", numSensors);
	printf("Ratiometric: %d\n", ratiometric);

	for (i = 0; i < numSensors; i++)
	{
		CPhidgetInterfaceKit_getSensorChangeTrigger(phid, i, &triggerVal);

		printf("Sensor#: %d > Sensitivity Trigger: %d\n", i, triggerVal);
	}

	return 0;
}
// ============ Call Backs IFK End =====================

// ============ Call Backs Servo Begin =================
int CCONV AttachHandler(CPhidgetHandle ADVSERVO, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(ADVSERVO, &name);
	CPhidget_getSerialNumber(ADVSERVO, &serialNo);
	printf("%s %10d attached!\n", name, serialNo);

	return 0;
}

int CCONV DetachHandler(CPhidgetHandle ADVSERVO, void *userptr)
{
	int serialNo;
	const char *name;

	CPhidget_getDeviceName(ADVSERVO, &name);
	CPhidget_getSerialNumber(ADVSERVO, &serialNo);
	printf("%s %10d detached!\n", name, serialNo);

	return 0;
}

int CCONV ErrorHandler(CPhidgetHandle ADVSERVO, void *userptr, int ErrorCode, const char *Description)
{
	printf("Error handled. %d - %s\n", ErrorCode, Description);
	return 0;
}

int CCONV PositionChangeHandler(CPhidgetAdvancedServoHandle ADVSERVO, void *usrptr, int Index, double Value)
{
	cout << Value;
	return 0;
}

//Display the properties of the attached phidget to the screen.  We will be displaying the name, serial number and version of the attached device.

int display_properties(CPhidgetAdvancedServoHandle phid)
{
	int serialNo, version, numMotors;
	const char* ptr;

	CPhidget_getDeviceType((CPhidgetHandle)phid, &ptr);
	CPhidget_getSerialNumber((CPhidgetHandle)phid, &serialNo);
	CPhidget_getDeviceVersion((CPhidgetHandle)phid, &version);

	CPhidgetAdvancedServo_getMotorCount(phid, &numMotors);

	printf("%s\n", ptr);
	printf("Serial Number: %10d\nVersion: %8d\n# Motors: %d\n", serialNo, version, numMotors);

	return 0;
}
// ============ Call Backs Servo End =================


int System_Steering_Control()
{
	int result;
	__int64 curr_pos;
	const char *err;
	int stopped;
	int state;

	CPhidgetStepperHandle stepper = 0;
	CPhidgetInterfaceKitHandle ifKit = 0;
	CPhidgetAdvancedServoHandle servo = 0;

	CPhidgetStepper_create(&stepper);
	CPhidgetInterfaceKit_create(&ifKit);
	CPhidgetAdvancedServo_create(&servo);


	CPhidget_set_OnAttach_Handler((CPhidgetHandle)stepper, AHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)stepper, DHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)stepper, EHandler, NULL);

	CPhidget_set_OnAttach_Handler((CPhidgetHandle)ifKit, attachHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)ifKit, detachHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)ifKit, errorHandler, NULL);

	CPhidget_set_OnAttach_Handler((CPhidgetHandle)servo, AttachHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)servo, DetachHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)servo, ErrorHandler, NULL);

	CPhidgetInterfaceKit_set_OnSensorChange_Handler(ifKit, sensorChangeHandler, NULL);
	CPhidgetInterfaceKit_set_OnInputChange_Handler(ifKit, inputChangeHandler, NULL);
	CPhidgetInterfaceKit_set_OnOutputChange_Handler(ifKit, outputChangeHandler, NULL);

	CPhidgetStepper_set_OnPositionChange_Handler(stepper, PCHandler, NULL);
	CPhidgetAdvancedServo_set_OnPositionChange_Handler(servo, PositionChangeHandler, NULL);

	//Hard coded serial numbers for the Phidgets in Use

	CPhidget_open((CPhidgetHandle)stepper, 420840);
	CPhidget_open((CPhidgetHandle)ifKit, 319699);
	CPhidget_open((CPhidgetHandle)servo, 392837);

	printf("Waiting for interface kit to be attached....");
	if ((result = CPhidget_waitForAttachment((CPhidgetHandle)ifKit, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		printf("Problem waiting for attachment: %s\n", err);
		return 0;
	}

	printf("Waiting for Phidget to be attached....");
	if ((result = CPhidget_waitForAttachment((CPhidgetHandle)stepper, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		printf("Problem waiting for attachment: %s\n", err);
		return 0;
	}


	if ((result = CPhidget_waitForAttachment((CPhidgetHandle)servo, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		printf("Problem waiting for attachment: %s\n", err);
		return 0;
	}

	DP(stepper);
	Display_Properties(ifKit);
	display_properties(servo);
	
	// Initialize Stepper and Servos
	//Parent loop starts here
		CPhidgetStepper_setAcceleration(stepper, 0, 20000);
		CPhidgetStepper_setVelocityLimit(stepper, 0, 11000);
		CPhidgetStepper_setCurrentLimit(stepper, 0, 1.2); // Current limit set to 1.2 mA

		CPhidgetAdvancedServo_setVelocityLimit(servo, 0, 316);
		CPhidgetAdvancedServo_setAcceleration(servo, 0, 182857.143);
		CPhidgetAdvancedServo_setEngaged(servo, 0, 1);

		CPhidgetAdvancedServo_setVelocityLimit(servo, 1, 45);
		CPhidgetAdvancedServo_setAcceleration(servo, 1, 29121);
		CPhidgetAdvancedServo_setEngaged(servo, 1, 1);

		CPhidgetAdvancedServo_setVelocityLimit(servo, 2, 45);
		CPhidgetAdvancedServo_setAcceleration(servo, 2, 29121);
		CPhidgetAdvancedServo_setEngaged(servo, 2, 1);

		CPhidgetAdvancedServo_setVelocityLimit(servo, 3, 45);
		CPhidgetAdvancedServo_setAcceleration(servo, 3, 29121);
		CPhidgetAdvancedServo_setEngaged(servo, 3, 1);

		if (CPhidgetStepper_getCurrentPosition(stepper, 0, &curr_pos) == EPHIDGET_OK)
			printf("Motor: 0 > Current Position: %lld\n", curr_pos);

		CPhidgetStepper_setCurrentPosition(stepper, 0, 0);
		CPhidgetStepper_setEngaged(stepper, 0, 1);

		joyStartStepper = 0;


		// End Initialize Stepper and Servos
	//	cout << "boolstepper:" << boolStepper << endl;
		
		do
		{
			if (boolStepper == 1)
			{
				CPhidgetStepper_setTargetPosition(stepper, 0, insDepth);
				CPhidgetStepper_getCurrentPosition(stepper, 0, &stepperCurrentPosition);
			}


			if (servoFlag0 == 1)
			{
				CPhidgetAdvancedServo_getPosition(servo, 0, &servo0CurrentPosition);
				CPhidgetAdvancedServo_setPosition(servo, 0, rotServo0);
			}
			else if (servoFlag1 == 1)
			{
				CPhidgetAdvancedServo_getPosition(servo, 1, &servo1CurrentPosition);
				CPhidgetAdvancedServo_setPosition(servo, 1, rotServo1);
			}

			if (servoFlag2 == 1)
			{
				CPhidgetAdvancedServo_getPosition(servo, 2, &servo2CurrentPosition);
				CPhidgetAdvancedServo_setPosition(servo, 2, rotServo2);
			}
			else if (servoFlag3 == 1)
			{
				CPhidgetAdvancedServo_getPosition(servo, 3, &servo3CurrentPosition);
				CPhidgetAdvancedServo_setPosition(servo, 3, rotServo3);
			}
		} while (boolEnable == 1);

			stopped = PFALSE;

			while (!stopped)
			{
				CPhidgetStepper_getStopped(stepper, 0, &stopped);
			}
			CPhidgetStepper_setEngaged(stepper, 0, 0);

			while (!stopped)
			{
				CPhidgetAdvancedServo_getStopped(servo, 0, &state);
				stopped = state;
			}

			while (!stopped)
			{
				CPhidgetAdvancedServo_getStopped(servo, 1, &state);
				stopped = state;
			}

			while (!stopped)
			{
				CPhidgetAdvancedServo_getStopped(servo, 2, &state);
				stopped = state;
			}

			while (!stopped)
			{
				CPhidgetAdvancedServo_getStopped(servo, 3, &state);
				stopped = state;
			}


			printf("Press any key to end\n");
			getchar();
		

			stopped = PFALSE;


			CPhidgetAdvancedServo_setEngaged(servo, 0, 0);
			CPhidgetAdvancedServo_setEngaged(servo, 1, 0);
			CPhidgetAdvancedServo_setEngaged(servo, 2, 0);
			CPhidgetAdvancedServo_setEngaged(servo, 3, 0);
		
	printf("Closing...\n");

	CPhidget_close((CPhidgetHandle)stepper);
	CPhidget_delete((CPhidgetHandle)stepper);
	CPhidget_close((CPhidgetHandle)servo);
	CPhidget_delete((CPhidgetHandle)servo);
	CPhidget_close((CPhidgetHandle)ifkit);
	CPhidget_delete((CPhidgetHandle)ifkit);

	return 0;
}

int dynamicTesting() {
	
	int result;
	const char *err;
	int stopped;
	int state, rotServo;
	int inputAngle;
	int servoNumber;
	cout << "Specify servo to actuate (0/1/2/3)" << endl;
	cin >> servoNumber;
	cout << "Specify Angle of Actuation:" << endl;
	cin >> inputAngle;
	CPhidgetAdvancedServoHandle servo = 0;
	CPhidgetInterfaceKitHandle ifKit = 0;

	CPhidgetAdvancedServo_create(&servo);
	CPhidgetInterfaceKit_create(&ifKit);

	CPhidget_set_OnAttach_Handler((CPhidgetHandle)ifKit, attachHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)ifKit, detachHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)ifKit, errorHandler, NULL);

	CPhidget_set_OnAttach_Handler((CPhidgetHandle)servo, AttachHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)servo, DetachHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)servo, ErrorHandler, NULL);

	CPhidgetInterfaceKit_set_OnSensorChange_Handler(ifKit, sensorChangeHandler, NULL);
	CPhidgetInterfaceKit_set_OnInputChange_Handler(ifKit, inputChangeHandler, NULL);
	CPhidgetInterfaceKit_set_OnOutputChange_Handler(ifKit, outputChangeHandler, NULL);
	
	CPhidgetAdvancedServo_set_OnPositionChange_Handler(servo, PositionChangeHandler, NULL);

	//Hard coded serial numbers for the Phidgets in Use

	CPhidget_open((CPhidgetHandle)stepper, 420840);
	CPhidget_open((CPhidgetHandle)ifKit, 319699);
	CPhidget_open((CPhidgetHandle)servo, 392837);


	CPhidget_set_OnAttach_Handler((CPhidgetHandle)servo, AttachHandler, NULL);
	CPhidget_set_OnDetach_Handler((CPhidgetHandle)servo, DetachHandler, NULL);
	CPhidget_set_OnError_Handler((CPhidgetHandle)servo, ErrorHandler, NULL);
	CPhidgetAdvancedServo_set_OnPositionChange_Handler(servo, PositionChangeHandler, NULL);

	//Hard coded serial numbers for the Phidgets in Use
	CPhidget_open((CPhidgetHandle)ifKit, 319699);
	CPhidget_open((CPhidgetHandle)servo, 392837);

	if ((result = CPhidget_waitForAttachment((CPhidgetHandle)servo, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		printf("Problem waiting for attachment: %s\n", err);
		return 0;
	}

	printf("Waiting for interface kit to be attached....");
	if ((result = CPhidget_waitForAttachment((CPhidgetHandle)ifKit, 10000)))
	{
		CPhidget_getErrorDescription(result, &err);
		printf("Problem waiting for attachment: %s\n", err);
		return 0;
	}

	Display_Properties(ifKit);
	display_properties(servo);
	CPhidgetAdvancedServo_setVelocityLimit(servo, servoNumber, 100);
	CPhidgetAdvancedServo_setAcceleration(servo, servoNumber, 182857.143);
	CPhidgetAdvancedServo_setEngaged(servo, servoNumber, 1);
	cout << runDynamics <<endl;

	while (runDynamics)
	{
		stopped = PFALSE;
		CPhidgetAdvancedServo_setPosition(servo, servoNumber, inputAngle);
		while (!stopped)
		{
			cout << "iter 1" << endl;
			CPhidgetAdvancedServo_getStopped(servo, servoNumber, &state);
			stopped = state;
		}

		stopped = PFALSE;
		CPhidgetAdvancedServo_setPosition(servo, servoNumber, 0);
		while (!stopped)
		{
			cout << "iter 2" << endl;
			CPhidgetAdvancedServo_getStopped(servo, servoNumber, &state);
			stopped = state;
		}
}
	
	
	CPhidgetAdvancedServo_setEngaged(servo, servoNumber, 0);
	CPhidget_close((CPhidgetHandle)servo);
	CPhidget_delete((CPhidgetHandle)servo);

}

int main(int argc, char* argv[])
{
	int userPreference;
	cout << "Please!" << endl;
	cout << " Please select mode of operation" << endl;
	cout << "[1] Steering Task" << endl;
	cout << "[2] Dynamic Testing - w/servo" << endl;
	cout << "[3] Exit" << endl;
	cin >> userPreference;

	if (userPreference == 1)
	{
		cout << "Steering Mode" << endl;
		System_Steering_Control();
	}
	else if(userPreference == 2)
	{
		cout << "Dynamic Testing" << endl;
		dynamicTesting();
	}
	else if (userPreference == 3)
	{
		exit;
	}
	return 0;
}


